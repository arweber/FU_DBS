from bs4 import BeautifulSoup
import requests
import psycopg2

def getPage(url):
    r = requests.get(url)
    data = r.text
    soupobj = BeautifulSoup(data, "lxml")
    return soupobj


def main():
    
    # Datenbankverbindung aufbauen.
    try:
        connection = psycopg2.connect("dbname='Heise' user='postgres' password = 'postgres' host='localhost'")
    except:
        print ('Keine Verbindung moeglich')
    cursor = connection.cursor();
    
    # Menge initialisieren, um die einzelnen Überschriften darin abzulegen
    s = set()
    
    for i in range(0,6):
    
        url = "https://www.heise.de/thema/https?seite="+str(i)
        
        # Inhalt der Webseite auslesen.
        content = getPage(url)
        
        for c in content.findAll("div", { "class" : "recommendation" }):
            # Überschriften auswählen. strip() entfernt Leerzeichen am Ende sowie \n-Zeichen.
            temp = c.find("header").contents[0].strip()
            # Entferne Sonderzeichen
            temp = temp.replace('"', '')
            temp = temp.replace(':', '')
            temp = temp.replace('!', '')
            temp = temp.replace('?', '')
            temp = temp.replace(' - ', ' ')
            # Hinzufügen zur Menge s. 
            s.add(temp)
    
    # Menge in eine Liste umwandeln => Ausgelesene Daten sind alle in dieser Datenstruktur gespeichert.
    s = list(s)
    
    # Überschriften in Wörter zerlegen, mittels einer Datenbank auswerten.
    for each in s:
        temp = each.split(" ")
        for t in temp:
            cursor.execute("SELECT anzahl FROM woerter WHERE wort=%s;", (t,))
            # Falls noch kein Eintrag für dieses Wort, trage das Wort ein; sonst setzte nur den Zähler hoch.
            if cursor.rowcount == 0:
                cursor.execute("INSERT INTO woerter(wort, anzahl) VALUES (%s, %s)",(t, 1,))
            else:
                cursor.execute("UPDATE woerter SET anzahl=anzahl+1 WHERE wort=%s", (t,))
    
    # Top 3 auslesen.
    cursor.execute("SELECT wort, anzahl FROM woerter ORDER BY anzahl DESC LIMIT 3")
    tupel = cursor.fetchall()
    
    string = "Die Top-3-Wörter in den Überschriften sind:\n"
    
    for t in tupel:
        string += (t[0]+" ("+str(t[1])+"), ")
    
    print(string)
    
    #connection.commit() # Nur nötig, wenn Daten in der Datenbank verbleiben sollen.
    connection.close()


if __name__ == '__main__':
    main()
